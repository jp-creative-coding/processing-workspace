# processing

[Processing](https://processing.org/) is a creative coding environment originated by [Casey Reis](http://reas.com/) and [Ben Fry](https://benfry.com/). It is now being developed and maintained by the [Processing Foundation](https://processingfoundation.org/).

Processing uses a sketchbook approach to encourage artists to code, and coders to art. This original version runs on Java.